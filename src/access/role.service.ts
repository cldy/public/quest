import { BaseService } from '../core/base.service'
import { Repository } from 'typeorm'
import { InjectRepository } from '@nestjs/typeorm'
import { Role } from './role.model'

export class RoleService extends BaseService<Role> {
    constructor (
        @InjectRepository(Role)
        protected repository: Repository<Role>,
    ) {
        super(repository)
    }
}