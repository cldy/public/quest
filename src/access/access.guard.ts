import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common'
import { GqlExecutionContext } from '@nestjs/graphql'
import { User } from '../user/user.model'
import { Action } from './action.entity'
import { Reflector } from '@nestjs/core'
import { AccessService } from './access.service'

@Injectable()
export class AccessGuard implements CanActivate {
    constructor (
        private reflector: Reflector,
        private accessService: AccessService,
    ) {
    }

    canActivate (context: ExecutionContext): boolean {
        const req = GqlExecutionContext.create(context).getContext().req
        const actions: Action<any>[] = this.reflector.get('actions', context.getHandler())

        if (!actions) {
            return true
        }

        const user: User = req.user

        if (!user) {
            return false
        }

        return actions.every(action => this.accessService.authorise(user, action.subject, action.action, action.fields))
    }
}