import { Injectable, OnModuleInit } from '@nestjs/common'
import { DeepPartial, Repository } from 'typeorm'
import { Permission } from './permission.model'
import { InjectRepository } from '@nestjs/typeorm'
import { Role } from './role.model'
import { UserService } from '../user/user.service'
import { User } from '../user/user.model'
import { Reflector } from '@nestjs/core'
import { permittedFieldsOf } from '@casl/ability/extra'
import { pick } from 'lodash'

@Injectable()
export class AccessService implements OnModuleInit {
    constructor (
        @InjectRepository(Permission)
        private permissions: Repository<Permission>,
        @InjectRepository(Role)
        private roles: Repository<Role>,
        private users: UserService,
        private reflector: Reflector,
        private userService: UserService,
    ) {
    }

    // TODO: Remove seed after a better way of creating roles and permissions has been implemented
    async onModuleInit () {
        if (!await this.roles.findOne({name: 'member'})) {
            await this.roles.save(this.roles.create({
                name: 'member',
            }))
        }

        if (!await this.roles.findOne({name: 'admin'})) {
            await this.roles.save(this.roles.create({
                name: 'admin',
            }))
        }
        const member = await this.roles.findOne({name: 'member'})
        const admin = await this.roles.findOne({name: 'admin'})

        // await this.permissions.save(this.permissions.create({
        //     action: 'read',
        //     subject: 'User',
        //     conditions: {
        //         id: '{{user.id}}'
        //     },
        //     role: member,
        // }))
        // await this.permissions.save(this.permissions.create({
        //     action: 'read',
        //     subject: 'User',
        //     role: admin,
        //     fields: ['id', 'username'],
        // }))
        //
        // await this.permissions.save(this.permissions.create({
        //     action: 'read.any',
        //     subject: 'User',
        //     role: admin,
        // }))

        // await this.permissions.save(this.permissions.create({
        //     action: 'read',
        //     subject: 'Role',
        //     role: admin,
        //     fields: ['id', 'name'],
        // }))

        if (!await this.users.findOne({username: 'user1'})) {
            await this.users.create({
                email: 'user1@example.com',
                username: 'user1',
                password: 'password',
                roles: [member],
            })
        }

        if (!await this.users.findOne({username: 'user2'})) {
            await this.users.create({
                email: 'user2@example.com',
                username: 'user2',
                password: 'password',
                roles: [admin],
            })
        }
    }

    async filter<T extends any> (user: User, data: T, action: string): Promise<DeepPartial<T> | T> {
        if (this.reflector.get('useAccess', Object.getPrototypeOf(data))) {
            const abilities = await this.userAbilities(user)
            const permittedFields = abilities.flatMap(ability => permittedFieldsOf(ability, action, data))
            const permittedData = pick<T>(data, permittedFields)

            return Object.fromEntries(Object.entries(permittedData).map(([key, value]) => {
                return [key, this.filter(user, value, action)]
            })) as unknown as DeepPartial<T>
        }

        if (Array.isArray(data)) {
            return await Promise.all(data.map(async value => await this.filter(user, value, action))) as T
        }

        return data
    }

    async userAbilities (user: User) {
        const userWithRoles = user.roles ? user : await this.userService.findById(user.id, {relations: ['roles']})
        return userWithRoles.roles.map(role => role.ability(userWithRoles))
    }

    async authorise (user: User, subject: any, action: string, fields?: string[]): Promise<boolean> {
        const userWithRoles = user.roles ? user : await this.userService.findById(user.id, {relations: ['roles']})
        return Boolean(userWithRoles.roles.some(role => role.authorise(userWithRoles, subject, action, fields)))
    }
}