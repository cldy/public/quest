import { forwardRef, Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { Permission } from './permission.model'
import { Role } from './role.model'
import { AccessService } from './access.service'
import { UserModule } from '../user/user.module'
import { PermissionService } from './permission.service'
import { RoleService } from './role.service'
import { AccessGuard } from './access.guard'
import { AccessInterceptor } from './access.interceptor'

@Module({
    imports: [
        TypeOrmModule.forFeature([
            Permission,
            Role,
        ]),
        forwardRef(() => UserModule),
    ],
    providers: [
        AccessService,
        PermissionService,
        RoleService,
        AccessGuard,
        AccessInterceptor,
    ],
    exports: [
        AccessService,
        PermissionService,
        RoleService,
    ],
})
export class AccessModule {
}