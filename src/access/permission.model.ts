import { Column, Entity, Index, ManyToOne } from 'typeorm'
import { BaseModel } from '../core/base.model'
import { Role } from './role.model'
import { User } from '../user/user.model'
import { interpolate } from '../core/core.utils'
import { Field, ObjectType } from '@nestjs/graphql'
import { GraphQLJSONObject } from 'graphql-type-json'

@Entity()
@ObjectType()
export class Permission extends BaseModel {
    @Column()
    action: string

    @Column()
    subject: string

    @Column({
        type: 'jsonb',
        nullable: true,
    })
    @Field(() => GraphQLJSONObject)
    conditions?: object

    @Column('varchar', {
        array: true,
        nullable: true,
    })
    @Field(() => Array(String))
    fields?: string[]

    @Column({
        default: false,
    })
    inverse: boolean

    @Column({
        nullable: true,
    })
    reason?: string

    @Index()
    @ManyToOne(() => Role, role => role.permissions)
    @Field(() => Role)
    role: Role

    getConditions (user: User) {
        return this.conditions && interpolate(this.conditions, {user: user})
    }
}