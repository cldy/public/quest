import { Column, Entity, ManyToMany, OneToMany } from 'typeorm'
import { Field, ObjectType } from '@nestjs/graphql'
import { BaseModel } from '../core/base.model'
import { Permission } from './permission.model'
import { User } from '../user/user.model'
import { Ability } from '@casl/ability'

@Entity()
@ObjectType()
export class Role extends BaseModel {
    @Column({unique: true})
    name: string

    @OneToMany(() => Permission, permission => permission.role, {eager: true})
    @Field(() => [Permission], {nullable: 'items'})
    permissions: Permission[]

    @ManyToMany(() => User)
    @Field(() => [User], {nullable: 'items'})
    users: User[]

    ability (user: User) {
        const permissions = this.permissions.map(permission => {
            permission.conditions = permission.getConditions(user)
            return permission
        })
        return new Ability(permissions)
    }

    authorise<T> (user: User, subject: T, action: string, fields?: string[]) {
        if (fields) {
            return fields.every(field => this.ability(user).can(action, subject, field))
        } else {
            return this.ability(user).can(action, subject)
        }
    }
}