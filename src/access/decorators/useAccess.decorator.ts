import { applyDecorators } from '@nestjs/common'

export const UseAccess = () => applyDecorators(addAccessMetadata)

const addAccessMetadata: ClassDecorator = <T extends Function> (target: T) => {
    Reflect.defineMetadata('useAccess', true, target.prototype)
}