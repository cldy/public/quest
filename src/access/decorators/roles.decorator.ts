import { applyDecorators } from '@nestjs/common'
import { JoinTable, ManyToMany } from 'typeorm'
import { Role } from '../role.model'
import { Field } from '@nestjs/graphql'

export const Roles = (): PropertyDecorator => applyDecorators(
    ManyToMany(() => Role) as PropertyDecorator,
    JoinTable() as PropertyDecorator,
    Field(() => [Role], {nullable: 'items'}),
)