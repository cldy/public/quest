import { SetMetadata } from '@nestjs/common'
import { Action } from '../action.entity'


export const Access = (...actions: Action<any>[]) => SetMetadata('actions', actions)