import { CallHandler, Injectable, NestInterceptor } from '@nestjs/common'
import { Observable } from 'rxjs'
import { map } from 'rxjs/operators'
import { GqlExecutionContext } from '@nestjs/graphql'
import { User } from '../user/user.model'
import { Reflector } from '@nestjs/core'
import { AccessService } from './access.service'

export interface Response<T> {
    data: T
}

@Injectable()
export class AccessInterceptor<T> implements NestInterceptor<T, Response<T>> {
    constructor (
        private reflector: Reflector,
        private accessService: AccessService,
    ) {
    }

    intercept (context: GqlExecutionContext, next: CallHandler): Observable<Response<T>> {
        return next.handle().pipe(map(data => {
            const user: User = GqlExecutionContext.create(context).getContext().req.user

            if (!user) {
                return data
            }

            if (Array.isArray(data)) {
                return data
                    .filter(entity => {
                        return this.reflector.get('useAccess', Object.getPrototypeOf(entity))
                            ? this.accessService.authorise(user, entity, 'read')
                            : entity
                    })
                    .map(entity => {
                        return this.accessService.filter(user, entity, 'read')
                    })
            }

            if (this.reflector.get('useAccess', Object.getPrototypeOf(data))) {
                return this.accessService.authorise(user, data, 'read') && this.accessService.filter(user, data, 'read')
            }

            return data
        }))
    }
}