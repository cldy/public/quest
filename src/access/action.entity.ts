import { BaseModel } from '../core/base.model'
import { Type } from '@nestjs/common'

export class Action<T extends Type<BaseModel>> {
    constructor (
        public action: string,
        public subject: T | string,
        public fields?: string[],
    ) {
    }
}