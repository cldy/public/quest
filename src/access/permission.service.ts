import { BaseService } from '../core/base.service'
import { Permission } from './permission.model'
import { Repository } from 'typeorm'
import { InjectRepository } from '@nestjs/typeorm'

export class PermissionService extends BaseService<Permission> {
    constructor (
        @InjectRepository(Permission)
        protected repository: Repository<Permission>,
    ) {
        super(repository)
    }
}