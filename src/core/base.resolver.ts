import { Type, UseGuards, UseInterceptors } from '@nestjs/common'
import { Args, ID, Mutation, Query, Resolver } from '@nestjs/graphql'
import pluralize from 'pluralize'
import { BaseModel } from './base.model'
import { BaseService } from './base.service'
import { FindConditions, FindManyOptions } from 'typeorm'
import { GraphQLJSONObject } from 'graphql-type-json'
import { JwtGuard } from '../auth/jwt.guard'
import { AccessGuard } from '../access/access.guard'
import { Access } from '../access/decorators/access.decorator'
import { Action } from '../access/action.entity'
import { CurrentUser } from '../auth/currentUser.decorator'
import { User } from '../user/user.model'
import { AccessInterceptor } from '../access/access.interceptor'

export function BaseResolver<T extends BaseModel> (classRef: Type<T>, createInput: any, updateInput: any): any {

    @Resolver({isAbstract: true})
    @UseInterceptors(AccessInterceptor)
    @UseGuards(JwtGuard, AccessGuard)
    abstract class BaseResolverHost {
        protected constructor (
            protected service: BaseService<T>,
        ) {
        }

        @Query(() => classRef, {
            name: `${classRef.name.toLowerCase()}`,
            nullable: true,
        })
        @Access(new Action('read', classRef))
        async findById (@Args('id', {type: () => ID}) id: string, @CurrentUser() user: User): Promise<T> {
            return this.service.findById(id)
        }

        @Query(() => [classRef], {
            name: `all${pluralize(classRef.name)}`,
            nullable: 'items',
        })
        @Access(new Action('list', classRef))
        async all (): Promise<T[]> {
            return this.service.findMany()
        }

        @Query(() => classRef, {
            name: `find${classRef.name}`,
            nullable: true,
        })
        @Access(new Action('read', classRef))
        async findOne (
            @Args('by', {
                type: () => GraphQLJSONObject,
                description: 'FindConditions',
            }) conditions: FindConditions<T>,
            @CurrentUser() user: User,
        ): Promise<T> {
            return this.service.findOne(
                Object.assign({}, conditions),
            )
        }

        @Query(() => [classRef], {
            name: `find${pluralize(classRef.name)}`,
            nullable: 'items',
        })
        @Access(new Action('list', classRef))
        async findMany (
            @CurrentUser() user: User,
            @Args('options', {
                type: () => GraphQLJSONObject,
                nullable: true,
                description: 'FindManyOptions',
            }) options?: FindManyOptions<T>,
        ): Promise<T[]> {
            return this.service.findMany(
                Object.assign({}, options),
            )
        }

        @Mutation(() => classRef, {
            name: `create${classRef.name}`,
        })
        @Access(new Action('create', classRef))
        async create (
            @Args({
                name: 'input',
                type: () => createInput,
            }) input: typeof createInput,
        ): Promise<T> {
            return this.service.create(input)
        }

        @Mutation(() => classRef, {
            name: `update${classRef.name}`,
        })
        @Access(new Action('update', classRef))
        async update (
            @Args('id') id: string,
            @Args({
                name: 'input',
                type: () => updateInput,
            }) input: typeof updateInput,
        ): Promise<T> {
            return this.service.update(id, input)
        }
    }

    return BaseResolverHost
}