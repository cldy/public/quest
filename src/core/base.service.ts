import { DeepPartial, FindConditions, FindManyOptions, FindOneOptions, Repository } from 'typeorm'
import { NotFoundException } from '@nestjs/common'

export abstract class BaseService<T> {
    protected constructor (
        protected repository: Repository<T>,
    ) {
    }

    findById (id: string, options?: FindOneOptions<T>): Promise<T> {
        return this.repository.findOne(id, options)
    }

    findOne (conditions: FindConditions<T>, options?: FindOneOptions<T>): Promise<T> {
        return this.repository.findOne(conditions, options)
    }

    findMany (options?: FindManyOptions<T>): Promise<T[]> {
        return this.repository.find(options)
    }

    create (data: DeepPartial<T>): Promise<T> {
        const user = this.repository.create(data)
        return this.repository.save(user)
    }

    async update (id: string, data: DeepPartial<T>): Promise<T> {
        const user = await this.findById(id)
        if (!user) {
            throw new NotFoundException(`Object with ID ${id} not found`)
        }
        await this.repository.update(id, data)
        return this.findById(id)
    }
}