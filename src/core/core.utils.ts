import { template } from 'lodash'

export const interpolate = (object: Record<string, any>, context: Record<string, any>) => {
    return Object.fromEntries(Object.entries(object).map(([key, value]) => {
        switch (typeof value) {
            case 'string': {
                return [key, template(value, {
                    interpolate: /{{([\s\S]+?)}}/g,
                })(context)]
            }
            case 'object': {
                return [key, interpolate(value, context)]
            }
            default: {
                return [key, value]
            }
        }
    }))
}