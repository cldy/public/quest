import { BeforeInsert, CreateDateColumn, DeleteDateColumn, PrimaryColumn, UpdateDateColumn, VersionColumn } from 'typeorm'
import { ulid } from 'ulid'
import { Field, GraphQLISODateTime, ID, Int, ObjectType } from '@nestjs/graphql'
import { UseAccess } from '../access/decorators/useAccess.decorator'

@ObjectType({
    isAbstract: true,
})
@UseAccess()
export abstract class BaseModel {
    @PrimaryColumn()
    @Field(() => ID)
    id: string

    @CreateDateColumn()
    @Field(() => GraphQLISODateTime)
    createdAt: string

    @UpdateDateColumn()
    @Field(() => GraphQLISODateTime)
    updatedAt: string

    @DeleteDateColumn()
    @Field(() => GraphQLISODateTime)
    deletedAt?: string

    @VersionColumn()
    @Field(() => Int)
    revision: number

    @BeforeInsert()
    generateId () {
        this.id = ulid()
    }
}

