import { Module } from '@nestjs/common'
import { AppController } from './app.controller'
import { AppService } from './app.service'
import { GraphQLModule } from '@nestjs/graphql'
import { TypeOrmModule } from '@nestjs/typeorm'
import { CoreModule } from './core/core.module'
import { UserModule } from './user/user.module'
import { AccessModule } from './access/access.module'
import { ConfigModule, ConfigType } from '@nestjs/config'
import { databaseConfig } from './config/database.config'
import { ConnectionOptions } from 'typeorm'
import { AuthModule } from './auth/auth.module'
import { authConfig } from './config/auth.config'

@Module({
    imports: [
        ConfigModule.forRoot({
            envFilePath: [`${process.env.NODE_ENV || 'development'}.env`, '.env'],
            isGlobal: true,
            load: [databaseConfig, authConfig],
        }),
        GraphQLModule.forRoot({
            installSubscriptionHandlers: true,
            autoSchemaFile: true,
            context: ({req}) => ({req}),
        }),
        TypeOrmModule.forRootAsync({
            useFactory: (config: ConfigType<typeof databaseConfig>) => ({
                type: config.type,
                host: config.host,
                port: config.port,
                username: config.username,
                password: config.password,
                database: config.database,
                schema: config.schema,
                synchronize: config.synchronize,
                autoLoadEntities: true,
            } as Partial<ConnectionOptions>),
            inject: [databaseConfig.KEY],
        }),
        CoreModule,
        AccessModule,
        UserModule,
        AuthModule,
    ],
    controllers: [AppController],
    providers: [AppService],
})
export class AppModule {
}
