import { Args, Mutation, Query, Resolver } from '@nestjs/graphql'
import { User } from '../user/user.model'
import { RegisterInput } from './register.input'
import { AuthService } from './auth.service'
import { Token } from './token.entity'
import { CurrentUser } from './currentUser.decorator'
import { UseGuards, UseInterceptors } from '@nestjs/common'
import { JwtGuard } from './jwt.guard'
import { AccessInterceptor } from '../access/access.interceptor'
import { Access } from '../access/decorators/access.decorator'
import { Action } from '../access/action.entity'

@Resolver('Auth')
export class AuthResolver {
    constructor (
        private authService: AuthService,
    ) {
    }

    @Mutation(() => String)
    async register (@Args('input') data: RegisterInput): Promise<string> {
        return this.authService.register(data)
    }

    @Mutation(() => Token)
    async login (@Args('username') username: string, @Args('password') password: string): Promise<Token> {
        return this.authService.login(username, password)
    }

    @Query(() => User)
    @UseGuards(JwtGuard)
    @Access(new Action('read', User))
    @UseInterceptors(AccessInterceptor)
    async me (@CurrentUser() user: User): Promise<User> {
        return user
    }
}