import { forwardRef, Inject, Injectable, UnauthorizedException } from '@nestjs/common'
import { UserService } from '../user/user.service'
import argon2 from 'argon2'
import { User } from '../user/user.model'
import { Token } from './token.entity'
import { JwtService } from '@nestjs/jwt'
import { RegisterInput } from './register.input'

@Injectable()
export class AuthService {
    constructor (
        @Inject(forwardRef(() => UserService))
        private userService: UserService,
        private jwtService: JwtService,
    ) {
    }

    async hash (plain: string): Promise<string> {
        return argon2.hash(plain, {type: argon2.argon2id})
    }

    async verifyHash (hash: string, plain: string): Promise<boolean> {
        return argon2.verify(hash, plain)
    }

    async validateUser (username: string, password: string): Promise<User> {
        const user = await this.userService.findOne({username})
        if (!user) {
            return null
        }
        if (user.password && await this.verifyHash(user.password, password)) {
            return user
        }
        return null
    }

    async getUserToken (user: User): Promise<Token> {
        const payload = {
            username: user.username,
            sub: user.id,
        }

        return {
            token: await this.jwtService.signAsync(payload),
            userId: user.id,
        }
    }

    async register (data: RegisterInput): Promise<string> {
        const user = await this.userService.create(data)
        return user.id
    }

    async login (username: string, password: string): Promise<Token> {
        const user = await this.validateUser(username, password)
        if (!user) {
            throw new UnauthorizedException('Invalid username or password')
        }
        return this.getUserToken(user)
    }
}