import { Inject, Injectable } from '@nestjs/common'
import { PassportStrategy } from '@nestjs/passport'
import { ExtractJwt, Strategy } from 'passport-jwt'
import { ConfigType } from '@nestjs/config'
import { authConfig } from '../config/auth.config'
import { UserService } from '../user/user.service'
import { User } from '../user/user.model'

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
    constructor (
        @Inject(authConfig.KEY)
        private config: ConfigType<typeof authConfig>,
        private userService: UserService,
    ) {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            ignoreExpiration: false,
            secretOrKey: config.publicKey,
        })
    }

    async validate (payload: any): Promise<User> {
        return this.userService.findById(payload.sub)
    }
}