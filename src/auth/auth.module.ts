import { forwardRef, Module } from '@nestjs/common'
import { AuthService } from './auth.service'
import { LocalStrategy } from './local.strategy'
import { UserModule } from '../user/user.module'
import { JwtModule } from '@nestjs/jwt'
import { PassportModule } from '@nestjs/passport'
import { ConfigModule, ConfigType } from '@nestjs/config'
import { authConfig } from '../config/auth.config'
import { JwtStrategy } from './jwt.strategy'
import { LocalGuard } from './local.guard'
import { JwtGuard } from './jwt.guard'
import { AuthResolver } from './auth.resolver'
import { AccessModule } from '../access/access.module'

@Module({
    imports: [
        forwardRef(() => UserModule),
        PassportModule,
        JwtModule.registerAsync({
            useFactory: (config: ConfigType<typeof authConfig>) => ({
                privateKey: config.privateKey,
                signOptions: {
                    expiresIn: config.jwtExpiresIn,
                    algorithm: 'RS256',
                },
            }),
            imports: [ConfigModule],
            inject: [authConfig.KEY],
        }),
        AccessModule,
    ],
    providers: [AuthService, LocalStrategy, JwtStrategy, LocalGuard, JwtGuard, AuthResolver],
    exports: [AuthService],
})
export class AuthModule {
}