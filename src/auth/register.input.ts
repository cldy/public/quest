import { InputType } from '@nestjs/graphql'

@InputType()
export class RegisterInput {
    email: string
    username: string
    password: string
}