import { forwardRef, Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { UserService } from './user.service'
import { User } from './user.model'
import { UserResolver } from './user.resolver'
import { AccessModule } from '../access/access.module'
import { AuthModule } from '../auth/auth.module'

@Module({
    imports: [
        TypeOrmModule.forFeature([User]),
        forwardRef(() => AccessModule),
        forwardRef(() => AuthModule),
    ],
    providers: [
        UserService,
        UserResolver,
    ],
    exports: [UserService],
})
export class UserModule {
}