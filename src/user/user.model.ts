import { Column, Entity } from 'typeorm'
import { HideField, ObjectType } from '@nestjs/graphql'
import { BaseModel } from '../core/base.model'
import { Role } from '../access/role.model'
import { Roles } from '../access/decorators/roles.decorator'

@Entity()
@ObjectType()
export class User extends BaseModel {
    @Column()
    username: string

    @Column()
    email: string

    @Column()
    @HideField()
    password: string

    @Roles()
    roles: Role[]
}
