import { Injectable } from '@nestjs/common'
import { DeepPartial, Repository } from 'typeorm'
import { InjectRepository } from '@nestjs/typeorm'
import { User } from './user.model'
import { BaseService } from '../core/base.service'
import { AuthService } from '../auth/auth.service'
import { RoleService } from '../access/role.service'

@Injectable()
export class UserService extends BaseService<User> {
    constructor (
        @InjectRepository(User)
        protected repository: Repository<User>,
        private authService: AuthService,
        private roleService: RoleService,
    ) {
        super(repository)
    }

    async create (data: DeepPartial<User>): Promise<User> {
        const user = this.repository.create({
            ...data,
            password: await this.authService.hash(data.password),
            roles: data.roles ?? [await this.roleService.findOne({name: 'member'})],
        })
        return this.repository.save(user)
    }
}
