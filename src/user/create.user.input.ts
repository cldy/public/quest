import { InputType } from '@nestjs/graphql'

@InputType()
export class CreateUserInput {
    email: string
    username: string
    password: string
}