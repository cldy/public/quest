import { Resolver } from '@nestjs/graphql'
import { User } from './user.model'
import { UserService } from './user.service'
import { BaseResolver } from '../core/base.resolver'
import { CreateUserInput } from './create.user.input'

@Resolver(User)
export class UserResolver extends BaseResolver(User, CreateUserInput, CreateUserInput) {
    constructor (
        protected service: UserService,
    ) {
        super(service)
    }
}