import { registerAs } from '@nestjs/config'

export const databaseConfig = registerAs('database', () => ({
    type: process.env.DB_TYPE || 'postgres',
    host: process.env.DB_HOST || 'localhost',
    port: parseInt(process.env.DB_PORT) || 5432,
    username: process.env.DB_USERNAME || 'quest',
    password: process.env.DB_PASSWORD || 'quest',
    database: process.env.DB_DATABASE || 'quest',
    schema: process.env.DB_SCHEMA || 'public',
    synchronize: Boolean(process.env.DB_SYNCHRONIZE) ?? process.env.NODE_ENV === 'development',
}))