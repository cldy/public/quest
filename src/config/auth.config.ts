import { registerAs } from '@nestjs/config'
import fs from 'fs'

export const authConfig = registerAs('auth', () => ({
    publicKey: process.env.AUTH_PUBLIC_KEY_PATH && fs.readFileSync(process.env.AUTH_PUBLIC_KEY_PATH, {encoding: 'utf8'}),
    privateKey: process.env.AUTH_PRIVATE_KEY_PATH && fs.readFileSync(process.env.AUTH_PRIVATE_KEY_PATH, {encoding: 'utf8'}),
    jwtExpiresIn: process.env.AUTH_JWT_EXPIRES_IN || '1d',
}))